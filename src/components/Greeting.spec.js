import { shallowMount } from "@vue/test-utils";
import Greeting from "./Greeting.vue";

describe("Greeting.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(Greeting, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
