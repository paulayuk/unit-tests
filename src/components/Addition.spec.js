import { mount } from '@vue/test-utils'
import Addition from './Addition.vue'

describe('Addition.vue', () => {
  test('clicking button increments the counter value by 1', () => {
    const wrapper = mount(Addition)
    expect(wrapper.text()).toContain('addition: 0')
    wrapper.find('button').trigger('click')
    expect(wrapper.text()).toContain('addition: 1')
  })
})
